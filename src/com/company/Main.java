package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //examMarks();
        useAccount();
    }

    public static void useAccount(){
        BankAccount account = new BankAccount(500,"Sue Owens");
        account.deposit(400);
        BankAccount account2 = new BankAccount(700,"Fred Owens");
        account2.withdraw(150);
        System.out.println("Balance is " + account.getBalance());
        System.out.println("All details: " + account);
        System.out.println(account2);
    }

    public static void examMarks(){
        Random rn = new Random();
        int[] examMarks = new int[10];
        for(int i = 0; i < examMarks.length; i++){
            examMarks[i] = 25 + rn.nextInt(75);
        }
        printGrades(examMarks);
        System.out.println("The top mark is " + bestMark(examMarks));
    }

    public static void printGrades(int[] marks){
        String grade = "";
        for(int i = 0; i < marks.length; i++){
            if(marks[i] >= 80){
                grade = marks[i] + " gets a grade A";
            }
            else if(marks[i] >= 60){
                grade = marks[i] + " gets a grade B";
            }
            else if(marks[i] >= 45){
                grade = marks[i] + " gets a grade C";
            }
            else{
                grade = marks[i] + " gets a resit";
            }
            System.out.println(grade);
        }

    }

    public static int bestMark(int[] marks){
        int topMark = marks[0];
        // Exercise
        for(int i = 1; i < marks.length; i++){
            if(marks[i] > topMark){
                topMark = marks[i];
            }
        }
        return topMark;
    }
}
