package com.company;

public class BankAccount {
    private int accountNumber;
    private double balance;
    private String holder;
    private static int nextID = 1;

    public void withdraw(double amount){
        this.balance -= amount;
    }

    public void deposit(double amount){
        this.balance += amount;
    }



    @Override
    public String toString() {
        return "BankAccount{" +
                "accountNumber=" + accountNumber +
                ", balance=" + balance +
                ", holder='" + holder + '\'' +
                '}';
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public BankAccount(double balance, String holder) {
        this.balance = balance;
        this.holder = holder;
        accountNumber = nextID++;
    }

    public BankAccount(){
        this(100,"Anonymous");
        accountNumber = nextID++;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }
}
